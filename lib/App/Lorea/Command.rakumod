unit class App::Lorea::Command:ver<0.2.7>;

use Terminal::ANSIColor;
constant RAINBOW = < red green yellow blue magenta cyan >;

my role Queue {
    method add  ( Str )             { ... }
    method take (     ) returns Str { ... }
}

my class Backlog does Queue {
    has SetHash $!queue;
    method add  ( Str $s where *.chars > 0 ) { $!queue{$s} = True }
    method take ( --> Str ) { $!queue ?? $!queue.grab !! '' }
}

my class Task does Queue {
    has Str $!task;
    method add  ( Str $s where *.chars > 0 ) { $!task = $s }
    method take ( --> Str ) {
        LEAVE $!task = '';
        return $!task;
    }
}

my $id = 0;
has Int $!id = $id++;

has Str         @!args       is built;
has Str         $!substitute is built;
has Supply      $!supply     is built;
has Bool        $!is-service is built;
has Bool        $!colours    is built;
has Bool        $!numbers    is built;
has Queue       $!queue;
has Proc::Async $!proc;
has Promise     $!done;
has &!decorator = sub ( $out, $msg ) { $out.print: $msg }

method new ( $args, *%extra ) {
    self.bless: args => $args.flat, |%extra;
}

method TWEAK () {
    $!queue = $!is-service.not || @!args (&) $!substitute
        ?? Backlog.new !! Task.new;

    &!decorator.wrap: -> $out, $msg {
        $out.&callwith( colored $msg.chomp, 'bold ' ~ RAINBOW[$!id + 1 % *] )
    } if $!colours;

    &!decorator.wrap: -> $out, $msg {
        $out.&callwith( sprintf '[%02d] %s', $!id, $msg )
    } if $!numbers;

    self!start if $!is-service;
}

method !say  ($msg) { &!decorator($*OUT, $msg) }
method !note ($msg) { &!decorator($*ERR, $msg) }

my Lock $sequential-lock;
method make-sequential { $sequential-lock ||= Lock.new }

method !start ( :$path --> Promise) {
    if try $!done.status ~~ Planned {
        # self!note: 'Command is already started and still running';
        return $!done
    }

    my @args = @!args;
    @args .= map: *.subst: $!substitute, $path if $path;

    $!proc .= new: @args;

    $!proc.stdout.tap: &!decorator.assuming($*OUT);
    $!proc.stderr.tap: &!decorator.assuming($*ERR);

    .lock with $sequential-lock;

    self!say('Starting service') if $!is-service;

    $!done = $!proc.start;
    $!done.then: { self!note('Service has finished') } if $!is-service;

    await $!done and .unlock with $sequential-lock;
    $!done;
}

method !restart ( :$path --> Promise ) {
    use Timer::Breakable;
    # note 'In restart';

    if try $!done.status ~~ Planned {
        &!decorator($*ERR, 'Killing service') if $!is-service;
        $!proc.kill: SIGTERM;

        my $timeout = Timer::Breakable.start: 1, {
            # Not working for now.
            # See https://github.com/rakudo/rakudo/issues/1701
            self!note: 'Process did not finish on time! Trying harder';
            $!proc.kill: SIGKILL;
        }

        await $!done.clone.then: { $timeout.stop }
    }

    self!start: :$path;
}

method !process-queue () {
    while $!queue.take -> $path {
        # note 'Restarting for ' ~ $path;
        await self!restart: :$path;

        CATCH {
            when X::Proc::Unsuccessful {
                when quietly .proc.signal == 9|15 {
                    # Process was terminated, likely by us. Nothing to see here
                }
            }
            default {
                my @args = @!args.map: *.subst: $!substitute, $path;
                self!note: "Encountered an error when running '{ @args.join: ' ' }";
                self!note: .message;
            }
        }
    }
}

method run ( --> Promise ) {
    use Timer::Stopwatch;
    my Timer::Stopwatch $timer .= new;

    start react {
        whenever $!supply {
            # True if we are still within the window
            $timer.reset: 0.3;
            $!queue.add: .path;
        }
        whenever $timer { start self!process-queue }
    }
}

=begin pod

=head2 NAME

App::Lorea::Command - A class representing a command to be run by lorea

=head2 SYNOPSIS

=begin code

use App::Lorea::Watcher;
use App::Lorea::Command;

my $supply = $path.&watch-recursive(
    debounce => 0.5,
    exclude  => rx{ '~' $ }, # Ignore files ending in ~
);

.run with App::Lorea::Command.new: @args, :$is-service, :$supply;

=end code

=head2 DESCRIPTION

This module represents a command to be executed by the C<lorea> command line
client. It makes little sense to use it outside of that context.

=head2 METHODS

=head3 new

=head4 args

A C<List> of arguments to pass to the underlying process.

=head4 is-service

A C<Bool> to specify if this command is a service or not. A service is a
command that is expected to be constantly running, and restarted on a
file-system change. The alternative is a command that is most normally
short-running, and is started on a file-system change.

=head4 supply

A C<Supply> of C<IO::Notification::Change> objects. After calling C<run>,
every string received sent to this C<Supply> will trigger be enqueued to
trigger a possible command restart.

See C<run> for an explanation of the queueing process.

=head3 run

=begin code
method run returns Promise
=end code

Start listening to events from the C<Supply> passed to the constructor.
Every event is expected to be a C<IO::Notification::Change>. File-system
changes will be queued and the queue will be processed after a 300
milliseconds have ellapsed since the last event.

How these queues work depends on whether this command represents a
"service" or not.

Queues for services only keep the last event that has come in. This means
that a set of events rapidly firing with less that 300 milliseconds between
each will trigger a single service restart.

Queues for non-service commands keep unique events, which are processed
in an unspecified order.

=head2 AUTHOR

José Joaquín Atria <jjatria@cpan.org>

=head2 COPYRIGHT AND LICENSE

Copyright 2020 José Joaquín Atria

This library is free software; you can redistribute it and/or modify it
under the Artistic License 2.0.

=end pod
